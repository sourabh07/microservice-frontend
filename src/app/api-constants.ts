export class ApiConstants {
    public static getHostname() : string {return "http://localhost:8080/api/" }; 
    public static getAllUsers() : string {return "database/back/all/"};
    public static getOneUserFromDatabase() : string {return "database/back/"};
    public static getStockForUser() : string {return "stock/stock/"};
    public static getWeatherForUser() : string {return "weather/weather/"};
    
}
