export class StockDto {
    public stockName : string;
    public lastPrice : number;
    public lastChange : number;
    public lastPeg : number;
    public lastDividend : number;
    public lastUpdated : Date;
}
