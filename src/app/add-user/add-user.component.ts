import { Component, OnInit } from '@angular/core';
import { UserModel } from '../user-model';
import { LocationModel } from '../location-model';
import { StocksModel } from '../stocks-model' ;

// FormsModule

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {

  addUserDetails : UserModel;
  locationCount = 0;
  stockCount = 0;
  locations : LocationModel[] = [];
  stocks : StocksModel[] = [];
  location : string ;
  stock : string;

  constructor() {
    this.addUserDetails = new UserModel();
  }

  ngOnInit() { 
  }

  addLocation(location: string){
    this.locations.push(new LocationModel(location));
    this.increaseLocation();
  }

  addStock(stock: string){
    this.stocks.push(new StocksModel(stock));
    this.increaseStockCount();
  }

  increaseLocation(){
    this.locationCount++;
    console.log(this.locationCount);
  }

  addToDB(){
    this.addUserDetails.locations = this.locations;
    this.addUserDetails.stocks = this.stocks;
    
  }

  increaseStockCount(){
    this.stockCount++;
  }

}
