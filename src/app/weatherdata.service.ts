import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { ApiConstants } from '../app/api-constants';
import { WeatherDto } from './weather-dto';

@Injectable({
  providedIn: 'root'
})
export class WeatherdataService {

  url: string;
  headers: any;

  constructor(private client : HttpClient) {
    this.headers = new HttpHeaders().set('content-type', 'application/json');
    this.headers = new HttpHeaders().set('Access-Control-Allow-Origin','*');
   }

  public getUserWeatherDetails(firstName: string) {
    this.url = "";
    this.url = ApiConstants.getHostname() +  ApiConstants.getWeatherForUser() + firstName;
    return this.client.get<WeatherDto>(this.url);
 }
}
