import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { ApiConstants } from '../app/api-constants';
import { UserModel } from './user-model';

@Injectable({
  providedIn: 'root'
})

export class UserdataService {
  users: any[];
  url: string;
  headers: any;

  constructor(private client: HttpClient) {
    this.headers = new HttpHeaders().set('content-type', 'application/json');
    this.headers = new HttpHeaders().set('Access-Control-Allow-Origin','*');
  }
  public getAllUsers() {
    this.url = ApiConstants.getHostname() +  ApiConstants.getAllUsers();
    // return this.client.get<string[]>(this.url)//.subscribe(result => this.users = result);
    return this.client.get(this.url);   
  }
  public getUserDetails(firstName: string) {
    this.url = "";
    this.url = ApiConstants.getHostname() +  ApiConstants.getOneUserFromDatabase() + firstName;
    return this.client.get<UserModel>(this.url);
 }
}
