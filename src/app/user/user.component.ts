import { Component, OnInit } from '@angular/core';
import { UserModel } from '../user-model';
import { StockdataService } from '../stockdata.service';
import { WeatherdataService } from '../weatherdata.service';
import { UserdataService } from '../userdata.service';
import { StockDto } from '../stock-dto';
import { WeatherDto } from '../weather-dto';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  locationCount = 0;
  stockCount = 0;
  checkStock : boolean = false;
  checkWeather : boolean = false;

  addUser : boolean = false;
  updateUser : boolean = false;

  users : string[] = [];
  user : string = "";
  usersFirstName : string[] = [];
  userStockDetails : StockDto[] = [];
  userWeatherDetails : WeatherDto[] = [];
  checkStockButton : string = "Check Stock";
  checkWeatherButton: string = "Check Weather";

  addUserButton : string = "Add User";
  updateUserButton : string = "Update User";


  constructor(private stockData : StockdataService, private weatherData : WeatherdataService, private userData : UserdataService, ) { 
    this.getAllUsersFromDB();
  }
  enableStock(){
    this.checkStock == false? this.checkStock = true : this.checkStock = false;
    this.checkStock == true? this.checkStockButton = "Hide Stock" : this.checkStockButton = "Check Stock" ;
  }
  
  enableWeather(){
    this.checkWeather == false? this.checkWeather = true : this.checkWeather = false;
    this.checkWeather == true? this.checkWeatherButton = "Hide Weather" : this.checkWeatherButton = "Check Weather" ;
  }

  enableAddUser(){
    this.addUser == false? this.addUser = true : this.addUser = false;
    this.addUser == true? this.addUserButton = "Hide Add User" : this.addUserButton = "Add User" ;
  }
  enableUpdateUser(){
    this.updateUser == false? this.updateUser = true : this.updateUser = false;
    this.updateUser == true? this.updateUserButton = "Hide Update User" : this.updateUserButton = "Update User" ;
  }


  getAllUsersFromDB(){
    this.userData.getAllUsers().subscribe(data => {
      this.users = data;
    })
  }
  getUserStockDetailsFromInternet(id: string){
    let x = id.split(" ");
    console.log(x)
    this.stockData.getUserStockDetails(x[0]).subscribe(data =>{
      this.userStockDetails = data;
    })
  }

  getUserWeatherDetailsFromInternet(id: string){
    let x = id.split(" ");
    console.log(x)
    this.weatherData.getUserWeatherDetails(x[0]).subscribe(data =>{
      this.userWeatherDetails = data;
    })
  }
  
  ngOnInit() {
  }

}
