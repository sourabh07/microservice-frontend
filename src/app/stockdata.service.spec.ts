import { TestBed, inject } from '@angular/core/testing';

import { StockdataService } from './stockdata.service';

describe('StockdataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StockdataService]
    });
  });

  it('should be created', inject([StockdataService], (service: StockdataService) => {
    expect(service).toBeTruthy();
  }));
});
