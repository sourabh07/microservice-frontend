export class WeatherDto {
    public weather_state_name : string;
    public weather_state_abbr : string;
    public wind_direction_compass : string;
    public wind_direction : string;
    public wind_speed : string;
    public air_pressure : string;
    public humidity : string;
    public visibility : string;
    public min_temp : string;
    public max_temp : string;
    public the_temp : string;
}
