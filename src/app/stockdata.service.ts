import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { ApiConstants } from '../app/api-constants';
import { StockDto } from './stock-dto';

@Injectable({
  providedIn: 'root'
})
export class StockdataService {
  url: string;
  headers: any;
  
  constructor(private client : HttpClient) { 
    this.headers = new HttpHeaders().set('content-type', 'application/json');
    this.headers = new HttpHeaders().set('Access-Control-Allow-Origin','*');
  }

  public getUserStockDetails(firstName: string) {
    this.url = "";
    this.url = ApiConstants.getHostname() +  ApiConstants.getStockForUser() + firstName;
    return this.client.get<StockDto>(this.url);
 }
}
