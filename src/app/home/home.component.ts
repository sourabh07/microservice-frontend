import { Component, OnInit } from '@angular/core';
import { UserdataService } from '../userdata.service';
import { UserModel } from '../user-model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {

  users : string[] = [];

  userDetails : UserModel;
  userDataAvaialable :boolean = false;
    // 'user1','user2'];

  constructor(private uData : UserdataService) { 
  }
  ngOnInit() {
    // this.uData.getAllUsers().subscribe(result => this.users = result);
    this.uData.getAllUsers().subscribe(data => {
      this.users = data;
  })
  }
  getUserDetails(user : string){
    let x = user.split(" ");
    this.uData.getUserDetails(x[0]).subscribe(data => {
      this.userDetails = data;
    })
    this.userDataAvaialable = true;
  }
}
