import { LocationModel } from './location-model';
import { StocksModel } from './stocks-model';

export class UserModel {
	public firstName: string;
	public lastName: string;
	public password: string;
	public stocks: StocksModel[] = [];
	public locations: LocationModel[] = [];

    // constructor(){
	// 	this.firstName = "";
	// 	this.lastName="";
	// 	this.password: string;
	// 	this.stocks = new StocksModel();
	// 	this.locations: LocationModel[] = [];
	// };
}